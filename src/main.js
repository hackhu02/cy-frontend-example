import Vue from 'vue'
import './plugins/axios'
import App from './App.vue'
import router from './router'
import retina from 'retinajs'
import VueRetina from 'vue-retina'

import VeeValidate from 'vee-validate'

Vue.use(VeeValidate)
Vue.config.productionTip = false
Vue.use(VueRetina, { retina })

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
